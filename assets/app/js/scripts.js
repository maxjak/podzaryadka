$( '.service-item__row' ).click( function() {
    let block = $( this ).closest('.service-item').find('.service-item__description-block');

    if ( block.css( 'display' ) === 'none' )
    {
        block.stop().slideDown( 'slow' );
    }
    else
    {
        block.stop().slideUp( 'slow' );
    }
});

$( document ).ready( function (){
    $( '.navigation' ).on( 'click', '.nav-link', function (event) {
        event.preventDefault();
        var id  = $( this ).attr( 'href' ),
            top = $( id ).offset().top;
        $( 'body,html' ).animate( { scrollTop: top }, 1000 );
    });
});
