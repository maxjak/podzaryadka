module.exports = {
    'frontend-app': {
        src: {
            fonts: [
                './assets/app/fonts/**/*.{ttf,eot,otf,woff,woff2,svg}',
            ],
            images: './assets/app/images/**/*.{jpg,jpeg,png,gif,svg}',

            css: './assets/app/css/**/*.scss',
            js: './assets/app/js/scripts.js'
        },

        dst: {
            build: './web',
            fonts: './web/fonts',
            css: './web/css',
            js: './web/js',
            images: './web/images',
        }
    }
};