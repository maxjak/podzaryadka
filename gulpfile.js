const fs = require('fs');
const gulp = require('gulp');
const sass = require('gulp-sass');
const minifyImg = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const plumber = require('gulp-plumber');
const flatten = require('gulp-flatten');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const autoprefixer = require('autoprefixer');
const flexbugs = require('postcss-flexbugs-fixes');
const csso = require('postcss-csso');
const sassTildeImporter = require('node-sass-tilde-importer');
const through = require('through2');
const path = require('path');
const process = require('process');

let modules = require( './gulp.settings' );

function fonts( module, vars )
{
    gulp
        .src( module.src.fonts )
        .pipe( plumber() )
        .pipe( flatten() )
        .pipe( gulp.dest( module.dst.fonts ) );
}

function css( module, vars )
{
    gulp
        .src( module.src.css)
        .pipe( plumber() )
        .pipe( sourcemaps.init() )
        .pipe( sass( vars.sassOptions ) )
        .pipe(
            postcss(
                vars.postCssOptions
            )
        )
        .pipe(
            sourcemaps.write(
                '.',
                {
                    sourceRoot: '/'
                }
            )
        )
        .pipe( gulp.dest( module.dst.css ) )
        .pipe( browserSync.stream() )
}

function images( module, vars )
{
    gulp
        .src( module.src.images )
        .pipe( plumber() )
        .pipe( minifyImg() )
        .pipe(
            gulp.dest(
                module.dst.images
            )
        );
}

function watch( module )
{
    gulp.watch(
        module.src.css
    )
        .on('all', function ( event, file ) {
            task( css, null, getCssVars(), file );
        });

    gulp.watch(
        module.src.fonts
    )
        .on('all', function ( event, file ) {
            task( fonts, null, null, file );
        });

    gulp.watch(
        module.src.images
    )
        .on('all', function ( event, file ) {
            task( images, null, null, file );
        });

    gulp.watch(
        module.src.js,
        gulp.series('refresh')
    );
}

function task( task, done = null, vars = null, filepath = null )
{
    for( var idx in modules )
    {
        if( !modules.hasOwnProperty( idx ) )
        {
            continue;
        }

        if( typeof filepath === "string" )
        {
            let ids = [];

            gulp
                .src( modules[ idx ].src[ task.name ] )
                .pipe( function( file )
                {
                    let idx2 = idx;

                    return through.obj( ( file, enc, cb ) => {

                        if( ids.indexOf( idx ) === -1 )
                        {
                            if( filepath === path.relative( process.cwd(), file.path) )
                            {
                                ids.push( idx2 );

                                task( modules[ idx2 ], vars );
                            }
                        }

                    return cb( null, file );
                })

                }( idx ));
        }
        else
            task( modules[ idx ], vars );
    }

    if( typeof done === "function" )
        done();
}

function getCssVars()
{
    return {
        'sassOptions': {
            importer: sassTildeImporter
        },
        'postCssOptions': [
            flexbugs(),

            autoprefixer({
                overrideBrowserslist: [
                    '>1%',
                    'last 4 versions',
                    'Firefox ESR',
                    'not ie < 9', // React doesn't support IE8 anyway
                ],
                flexbox: 'no-2009',
            }),

            csso(),
        ],
    };
}

gulp.task('images', done => {
    task( images, done );
});

gulp.task( "fonts", done => {
    task( fonts, done );
});

gulp.task('css', done => {
    task( css, done, getCssVars() );
});

gulp.task('refresh', done => {
    browserSync.reload();

    done();
});

gulp.task( 'watch', () => {
    browserSync.init({
        proxy: 'localhost:8000',
        port: 3000,
        open: false,
    });

    task( watch );
});


gulp.task('build', gulp.series('fonts', 'css', 'images'));

gulp.task('default', gulp.series('build', 'watch'));
