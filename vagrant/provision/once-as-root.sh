#!/usr/bin/env bash

#== Import script args ==

timezone=$(echo "$1")

#== Bash helpers ==

function info {
  echo " "
  echo "--> $1"
  echo " "
}

#== Provision script ==

info "Provision-script user: `whoami`"

export DEBIAN_FRONTEND=noninteractive

info "Configure timezone"
timedatectl set-timezone ${timezone} --no-ask-password

info "Prepare root password for MySQL"
debconf-set-selections <<< "mysql-community-server mysql-community-server/root-pass password \"''\""
debconf-set-selections <<< "mysql-community-server mysql-community-server/re-root-pass password \"''\""
echo "Done!"

info "Add Repos"
apt-get install -y python-software-properties
apt-get install -y software-properties-common
add-apt-repository -y ppa:ondrej/php
apt-key adv -y --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
add-apt-repository -y 'deb [arch=amd64,i386,ppc64el] https://mirror.timeweb.ru/mariadb/repo/10.2/ubuntu xenial main'

wget https://nginx.org/keys/nginx_signing.key
apt-key add nginx_signing.key
add-apt-repository -y 'deb https://nginx.org/packages/ubuntu/ xenial nginx'
add-apt-repository -y 'deb-src https://nginx.org/packages/ubuntu/ xenial nginx'

add-apt-repository -y ppa:chris-lea/redis-server

info "Update OS software"
apt-get update -y
apt-get upgrade -y

info "Installing Redis"
apt-get install -y redis-server

info "Installing SuperVisor"
apt-get install -y supervisor
ln -s /app/vagrant/supervisor/queue.conf /etc/supervisor/conf.d/queue.conf

info "Install additional software"
apt-get install -y --force-yes php7.1-curl php7.1-cli php7.1-intl php7.1-mysqlnd php7.1-gd php7.1-fpm php7.1-mbstring php7.1-xml unzip nginx mariadb-server php.xdebug php7.1-imagick

info "Configure MySQL"
sed -i "s/.*bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
mysql -uroot <<< "CREATE USER 'root'@'%' IDENTIFIED BY ''"
mysql -uroot <<< "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'"
mysql -uroot <<< "DROP USER 'root'@'localhost'"
mysql -uroot <<< "FLUSH PRIVILEGES"
echo "Done!"

info "Configure PHP-FPM"
sed -i 's/user = www-data/user = vagrant/g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' /etc/php/7.1/fpm/pool.d/www.conf
sed -i 's/owner = www-data/owner = vagrant/g' /etc/php/7.1/fpm/pool.d/www.conf
cat << EOF > /etc/php/7.1/mods-available/xdebug.ini
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_autostart=1
EOF
echo "Done!"

info "Configure NGINX"
sed -i 's/user www-data/user vagrant/g' /etc/nginx/nginx.conf
sed -i 's/user  nginx/user vagrant/g' /etc/nginx/nginx.conf
echo "Done!"

info "Enabling site configuration"
ln -s /app/vagrant/nginx/app.conf /etc/nginx/conf.d/app.conf
echo "Done!"

info "Enabling SSL"
ln -s /app/vagrant/nginx/ssl /etc/nginx/ssl
#openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout /etc/nginx/ssl/domain.key -out /etc/nginx/ssl/domain.crt
#openssl dhparam -out /etc/nginx/ssl/dhparam.pem 4096
echo "Done!"

info "Initailize databases for MySQL"
mysql -uroot <<< "CREATE DATABASE yii2advanced DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;"
echo "Done!"

info "Install composer"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
