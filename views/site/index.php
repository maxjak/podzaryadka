<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>


<div class="podzaryadka">
    <header class="header">
        <div class="container header__container">
           <div class="header__block">
               <a href="#" class="header__logo-link">
                   <img src="../images/header__logo.svg" alt="" class="header__logo-image">
               </a>

               <div class="header__contacts">
                   <a href="#" class="header__number-link">
                       <span class="orange">+7 (8332)</span> 26-68-80
                   </a>

                   <div class="header__contacts-time">
                       ежедневно с 10:00 до 22:00
                   </div>
               </div>
           </div>
        </div>
    </header>

    <section class="navigation">
        <div class="navigation__container container">
            <nav class="navbar navigation__navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav navigation__nav">
                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#1">
                                Игровой комплекс
                                «Лабиринт»
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#2">
                                Батутный и
                                верёвочный парк
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#3">
                                Призовые
                                автоматы
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#4">
                                Виртуальная
                                реальность
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#5">
                                Party-Room
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#6">
                                Кафе
                            </a>
                        </li>

                        <li class="nav-item navigation__nav-item">
                            <a class="nav-link navigation__nav-link" href="#7">
                                Афиша
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </section>
    
    <section class="invite">
        <div class="container invite__container">
            <div class="invite__image-block">
                <img src="../images/invite__image.jpg" alt="" class="invite__image">
            </div>
            
            <div class="invite__text">
                <p>
                     <span class="bold">
                        Приглашаем детей и их родителей весело и активно провести время в семейном развлекательном центре «ПодZарядка»!
                     </span><br>
                    На огромной площади более 2000 квадратных метров расположены разнообразные игровые зоны: четырёхэтажный лабиринт с тюбинговыми горками и отдельной игровой зоной для малышей, батутная арена, скалодромы и множество детских призовых автоматов. Здесь можно лазить по стенам, летать на тарзанке, кататься с горок и бродить по виртуальной реальности!!!  Получив массу положительных эмоций, вы сможете  пообедать в расположенном здесь же уютном кафе и побаловать себя и своего ребёнка пиццей, блинами, картофелем фри, свежевыжатыми соками и коктейлями.
                </p>

                <p>
                    Ну а если у вас намечается торжество, то отметить его вы тоже можете здесь. Для празднования дней рождений, выпускных или других ярких событий работают «Party Rooms» - комнаты для проведения банкетов. Словом, в «ПодZарядке» вы найдёте всё, что необходимо для активного и увлекательного отдыха детей и взрослых в любое время года и в любую погоду!
                </p>

               <p>
                   <span class="bold">«ПодZарядка»</span> - энергия отличного настроения!
               </p>
            </div>
        </div>
    </section>

    <section class="service">
        <div class="service__container container">
            <div class="service-item" id="1" style="background-color: #ffe3c8">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_1.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #1180c6;">
                                Игровой
                                комплекс
                            </div>
                        </div>
                        <div class="service-item__name" style="color: #1180c6;">
                            «Лабиринт»
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_1.png" class="service-item__image">
                    </div>
                </div>

                <div class="service-item__description-block">
                    <div class="service-item__block">
                        <div class="service-item__main-title">
                            Включает в себя четырёхэтажный лабиринт, троллей
                            и малышковую площадку.
                        </div>

                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Детский лабиринт
                            </div>

                            <div class="service-item__text-description">
                                Четыре этажа детского лабиринта специально оборудованы для игр, которые никогда не наскучат! Целых две горки: тюбинговая и большая трёхполосная пластиковая горка, поролоновая яма с мягкими кубиками, разноцветные мостики, лестница-«радуга», разноцветные сферы, подвесные груши, вращающиеся валики, напольные препятствия и сетчатый тоннель. Все эти элементы не дадут скучать вашим маленьким непоседам. Здесь же расположено оборудование для спортивных игр - баскетбольное кольцо, дартс, футбольные ворота, а также небольшой батут. Возрастное ограничение – <strong>от 4 до 10 лет</strong>. Вместимость – до 70 человек. Здесь же можно поиграть на игровой приставке x-box.
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Детский лабиринт
                            </div>

                            <div class="service-item__text-description">
                                Четыре этажа детского лабиринта специально оборудованы для игр, которые никогда не наскучат! Целых две горки: тюбинговая и большая трёхполосная пластиковая горка, поролоновая яма с мягкими кубиками, разноцветные мостики, лестница-«радуга», разноцветные сферы, подвесные груши, вращающиеся валики, напольные препятствия и сетчатый тоннель. Все эти элементы не дадут скучать вашим маленьким непоседам. Здесь же расположено оборудование для спортивных игр - баскетбольное кольцо, дартс, футбольные ворота, а также небольшой батут. Возрастное ограничение – <strong>от 4 до 10 лет</strong>. Вместимость – до 70 человек. Здесь же можно поиграть на игровой приставке x-box.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Детский лабиринт
                            </div>

                            <div class="service-item__text-description">
                                Четыре этажа детского лабиринта специально оборудованы для игр, которые никогда не наскучат! Целых две горки: тюбинговая и большая трёхполосная пластиковая горка, поролоновая яма с мягкими кубиками, разноцветные мостики, лестница-«радуга», разноцветные сферы, подвесные груши, вращающиеся валики, напольные препятствия и сетчатый тоннель. Все эти элементы не дадут скучать вашим маленьким непоседам. Здесь же расположено оборудование для спортивных игр - баскетбольное кольцо, дартс, футбольные ворота, а также небольшой батут. Возрастное ограничение – <strong>от 4 до 10 лет</strong>. Вместимость – до 70 человек. Здесь же можно поиграть на игровой приставке x-box.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-item" id="2" style="background-color: #f8ffff">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_2.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #c40009;">
                                Батутный и
                                верёвочный
                                парк
                            </div>
                        </div>
                        <div class="service-item__name" style="color: #c40009;">
                            NonStop
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_2.png" class="service-item__image">
                    </div>
                </div>

                <div class="service-item__description-block">
                    <div class="service-item__block">
                        <div class="service-item__main-title">
                            27 совмещенных горизонтальных и вертикальных батутов,
                            батутная дорожка, стена восхождения, скалодром, поролоновая яма,
                            зона разминки, а также фотозона-пьедестал — все это уникальный
                            симбиоз развлечения и спорта.
                        </div>

                        <div class="service-item__second-title service-item__main-title">
                            Профессиональные тренеры. Надёжное сертифицированное
                            оборудование.
                        </div>

                        <div class="service-item__second-title service-item__main-title">
                            Ваши прыжки будут выше, тренировки эффективней,
                            а удовольствие — больше!
                        </div>

                        <div class="service-item__main-title red">
                            Телефон для записи и более подробной информации:
                            (8332) 780-730
                        </div>

                        <div class="service-item__second-title service-item__main-title">

                        </div>

                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Батутная арена
                            </div>

                            <div class="service-item__text-description">

                                Если вы хотите избавиться от лишних килограммов, научиться делать на батутах крутые трюки, весело и активно отдыхать  большой компанией или отмечать дни рождения — вам сюда!
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-description">
                                <strong>Фитнес на батутах</strong> — это сочетание аэробных (кардио) нагрузок, упражнений на формирование и укрепление основных групп мышц, комплекса на растяжку. Классическая тренировка длится до 60 минут, из них 40 минут —
                                с упором на тренировку сердечной мышцы, оставшееся время — упражнения силового характера и на растяжку.
                                Тренировки <a href="https://www.instagram.com/explore/tags/JumpFitness/" class="service-item__text-link">#JumpFitness</a> проходят с понедельника по пятницу в 20:00
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">

                            <div class="service-item__text-description">
                                <strong>Персональный тренинг</strong> - это отличная возможность достичь новые цели.
                                Вы можете отработать акробатические элементы, которые входят в состав многих видов спорта, поставить трюки для сноуборда или вейкборда, паркура и просто порадовать свою душеньку

                                - длительность занятия: 60 мин
                                - ограничения по весу: до 120 кг
                                - ограничения по возрасту: с 3 лет
                            </div>
                        </div>
                        <div class="service-item__text-block">

                            <div class="service-item__text-description">
                               <strong>Групповые тренировки.</strong> Весь «секрет» групповых тренировок в том, что у занимающихся всегда возникают дружеские взаимоотношения, «чувство локтя», а также дух здорового азарта и соперничества.
                                Группа занимается под пристальным вниманием тренера и получает всё необходимое для достижения намеченных целей.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-description">
                                <strong>Тимбилдинг.</strong> Батутный парк NONSTOP организует и проведет для вас и вашей компании, фирмы или офиса тимбилдинг, который вы запомните навсегда!
                                В программу входит: организованное мероприятие с  тренерами и инструкторами нашего парка, которое поможет вашей команде выйти на новый уровень.
                                Испытания, приготовленные нами, превзойдут Ваши ожидания - это будет весело, интересно, а главное - полезно и безопасно.
                            </div>

                            <div class="row">
                                <div class="col-lg-5 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-description">
                                <strong>День рождения.</strong> Не знаете, как отметить день рождения? Хватайте друзей и приходите в наш Батутный парк! Здесь работают профессиональные тренеры и весёлые аниматоры!
                                Вас ждут: веселье на батутах, спортивные эстафеты, интересные конкурсы, мастер-класс от наших тренеров, а также увлекательные программы от аниматоров.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Верёвочная трасса и скалодромы
                            </div>

                            <div class="service-item__text-description">
                                Эту трассу мы подготовили для самых отважных и смелых путешественников.
                                Она поможет Вам прокачать свою физическую форму и выносливость в игровой форме, подготовиться к прохождению самых трудных участков вашего пути.
                                Если Вы хотите померяться силами и ловкостью с друзьями или коллегами!
                                У нас можно легко спланировать корпоративный и дружеский выход на трассу
                                Беспокоитесь о безопасности?
                                Мы контролируем исправность всех элементов, а прохождение препятствий осуществляется обязательно в страховочных системах и касках.<br>

                                <strong>С нами Вы сможете покорить Эверест!</strong>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-item" id="3" style="background-color: #effad8">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_3.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #85da00;">
                                Призовые
                                автоматы
                            </div>
                        </div>
                        <div class="service-item__name" style="color: #85da00;">
                            Happy Quak
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_3.png" class="service-item__image">
                    </div>
                </div>

                <div class="service-item__description-block">
                    <div class="service-item__block">
                        <div class="service-item__text-block">
                            <div class="service-item__text-description">
                                На площадке представлено более 20 различных игровых автоматов: аэрохоккеи, силомер, гоночный симулятор, весёлые шарики, баскетбол и другие. С их помощью ребёнок развивает координацию и моторику, становится устойчив к стрессам, и даже учится воспитывать в себе силу воли. На всех автоматах можно заработать призовые жетоны и обменять их на подарок в призотеке.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-item" id="4" style="background-color: #dcdcdc">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_4.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #264897;">
                                Аттракцион
                            </div>
                        </div>
                        <div class="service-item__name" style="color: #264897;">
                            Виртуальная
                            реальность
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_4.png" class="service-item__image">
                    </div>
                </div>

                <div class="service-item__description-block">
                    <div class="service-item__block">
                        <div class="service-item__main-title">
                            Самый популярный аттракцион нашего времени!
                            Полное погружение в виртуальный мир!
                        </div>

                        <div class="service-item__text-block">
                            <div class="service-item__text-title">
                                Выбрав одну из игр, вы сможете:
                            </div>

                            <div class="service-item__text-description">
                                -  Сразиться с космическими дронами, которые пытаются захватить ваш корабль;<br>
                                -  Поездить за рулем настоящего болида формулы 1, гоночного автомобиля или грузовика;<br>
                                -  Полетать на штурмовике, космическом корабле, вертолете и в аэрокостюме;<br>
                                -  Покататься на американских горках, погрузиться в глубины океана, побывать в космосе;<br>
                                -  Сразиться с армией разъяренных зомби с полным ощущением наличия различного оружия в руках;<br>
                                -  Побывать в роли трансформера, который умеет бегать, летать и стрелять;<br>
                                -  Побродить по тёмным коридорам, пощекотать свои нервы, проверить себя на наличие страхов и фобий;<br>
                                - Получить образовательную информацию о происхождении нашей вселенной, побродить
                                по музеям и историческим местам. <br>
                            </div>

                            <div class="service-item__text-description">
                                Для виртуальной реальности возраст не имеет значения! Не обязательно быть любителем компьютерных игр для того, чтобы ощутить всё великолепие виртуальной реальности.
                                Наши развлечения ориентированы абсолютно на всех! Приходите сами и берите с собой друзей!
                                У вас будет возможность понаблюдать за реакцией друга на виртуальную реальность,
                                а также совместно поиграть в сетевые игры.
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-item" id="5" style="background-color: #fde5e5">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_5.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #c30080;">
                                банкетный зал
                            </div>
                        </div>
                        <div class="service-item__name" style="color: #c30080;">
                            Party-Room
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_5.png" class="service-item__image">
                    </div>
                </div>

                <div class="service-item__description-block">
                    <div class="service-item__block">
                        <div class="service-item__main-title">
                            Самые весёлые и яркие дни рождения отмечают здесь!

                        </div>

                        <div class="service-item__text-block">
                            <div class="service-item__text-description">
                                У нас вы можете арендовать небольшой банкетный зал (Party-Room), в котором вам будет удобно и просто накрыть столы для своих гостей. Таких залов в «ПодZарядке» два. Один – <strong>вместимостью до 25 человек</strong>, а второй чуть меньше. При желании, залы можно объединить, и устроить <strong>большой праздник на 40-50 человек.</strong>
                            </div>

                            <div class="service-item__text-description">
                                Что касается угощений. Здесь вам  предоставят выбор: принести все продукты с собой и накрыть праздничный стол по своему усмотрению, сделать заказ на обслуживание в расположенном здесь же кафе, которое славится своими вкусными блинами, пиццей и роллами, или же заказать кейтринговое обслуживание вашего торжества. Кстати, свечи для торта и воздушные шары на праздник можно приобрести прямо здесь, в специальном отделе развлекательного центра.
                            </div>

                            <div class="service-item__text-description">
                                Приятный бонус! При аренде Банкетного зала ваш именинник сможет пройти в игровой комплекс "Лабиринт" абсолютно бесплатно, а его маленькие гости (если их будет 10 человек) – <strong>со скидкой 20%!</strong>
                            </div>

                            <div class="service-item__text-description">
                                Ну а если вы захотите пригласить на день рождения какого-нибудь сказочного героя, или же устроить настоящее научное, мыльное  или какое-нибудь другое шоу, вам обязательно помогут с выбором аниматора. Поверьте, праздник, проведённый в «ПодZарядке», надолго останется в памяти вашего именинника и всех его гостей!
                            </div>

                            <div class="row">
                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_1.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_2.jpg" alt="" class="service-item__text-image">
                                </div>

                                <div class="col-lg-4 service-item__image-block">
                                    <img src="../images/service-item__image-block_3.jpg" alt="" class="service-item__text-image">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="service-item" id="6" style="background-color: #98d9d6">
                <div class="row service-item__row">
                    <div class="col-lg-5">
                        <div class="service-item__type">
                            <img src="../images/service__icon_5.png" class="service-item__icon">
                            <div class="service-item__description" style="color: #ffffff;">

                            </div>
                        </div>
                        <div class="service-item__name" style="color: #ffffff;">
                            КАФЕ
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <img src="../images/service__image_5.png" class="service-item__image">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="poster" id="7">
        <div class="container poster__container">
            <div class="poster__block">
                <div class="poster__title">
                    афиша
                </div>
            </div>

            <div class="poster__image-block">
                <img src="../images/poster.jpg" class="poster__image">
            </div>
        </div>
    </section>
</div>




