const path = require('path');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

let modules = require( './gulp.settings' );

var entries = {};

for( var idx in modules ) {
    if( !modules.hasOwnProperty( idx ) )
        continue;

    m = modules[ idx ];

    entries[ m.dst.js + '/app' ] = m.src.js
}

module.exports = {
    entry: entries,

    output: {
        filename: '[name].js',
        path: path.resolve( __dirname )
    },

    plugins: [
        new HardSourceWebpackPlugin(),
        new FriendlyErrorsWebpackPlugin({
            clearConsole: false,
        })
    ],

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }
        ]
    }
}
